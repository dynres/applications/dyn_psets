#!/bin/sh

echo "*******************************************************"
echo "Let's build the Dynamic PSets Library!"
echo "*******************************************************"
echo ""

autoreconf -i

echo "autogen: SUCCESS! You can now run ./configure [--prefix=/path/to/install-dir] [--with-mpi=/path/to/mpi]"