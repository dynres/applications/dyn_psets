# Dynamic PSets Library
This repository contains the source code for the dynamic PSets library (Dyn_PSets).
Dyn_PSets follows the design of [DMR](https://gitlab.bsc.es/siserte/dmr).

## Prerequisites
- [Open-MPI with DPP](https://gitlab.inria.fr/dynres/dyn-procs/ompi)

## Installation
The library can be installed with autotools. In this directory:

1. Run autogen
```
./autogen.sh
```
2. Run configure (optionally specify an install prefix and the path to mpi if they are not installed in a standard location)
```
./configure [--prefix=/path/to/install-dir] [--with-mpi=/path/to/mpi]
```
3. Run make
```
make && make install
```