#include "mpi.h"
#include "dyn_psets_internal.h"
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

dyn_pset_config_t * dyn_pset_config_new(){
    dyn_pset_config_t * config = (dyn_pset_config_t *) malloc(sizeof(dyn_pset_config_t));
    config->gc = true;
    return config;
}

void dyn_pset_config_free(dyn_pset_config_t ** config ){
    free(*config);
    return;
}

dyn_pset_data_t * dyn_pset_data_new(const char *pset, dyn_psets_expand_send_func_t exp_send, dyn_psets_expand_recv_func_t exp_recv, dyn_psets_shrink_send_func_t shr_send, dyn_psets_shrink_recv_func_t shr_recv){
    int flag = 0;
    char boolean_string[10];
    char **keys;
    MPI_Info info;
    
    dyn_pset_data_t *data = (dyn_pset_data_t *) malloc(sizeof(dyn_pset_data_t));
    data->session = MPI_SESSION_NULL;
    data->info = MPI_INFO_NULL;
    data->request = MPI_REQUEST_NULL;
    data->setop_pending = false;
    data->is_dynamic = false;
    data->output_psets = NULL;
    data->noutput_psets = 0;
    data->exp_send = exp_send;
    data->exp_recv = exp_recv;
    data->shr_send = shr_send;
    data->shr_recv = shr_recv;
    data->config = dyn_pset_config_new();
    strcpy(data->main_pset, pset);
    strcpy(data->delta_add, pset);
    strcpy(data->delta_sub, "");
    gethostname(data->hostname, 255);
    int primary = 0;

    v_print(3, "DYN_PSET: Session Init\n");
    MPI_Session_init(MPI_INFO_NULL, MPI_ERRORS_ARE_FATAL, &data->session);
    v_print(3, "DYN_PSET: Session Inited\n");
    /* Get the info from our mpi://WORLD pset */
    strcpy(data->main_pset, pset);
    v_print(5, "DYN_PSET: MPI_Session_get_pset_info\n");
    MPI_Session_get_pset_info (data->session, data->main_pset, &info);
    v_print(5, "DYN_PSET: MPI_Session_get_pset_info finished\n");

    MPI_Info_get(info, "mpi_primary", 6, boolean_string, &flag);
    primary = 0;
    if(0 == strcmp(boolean_string, "True")){
        primary = 1;
    } 
    /* get value for the 'mpi_dyn' key -> if true, this process was added dynamically */
    MPI_Info_get(info, "mpi_dyn", 6, boolean_string, &flag);

    /* if mpi://WORLD is a dynamic PSet retrieve the name of the main PSet stored on mpi://WORLD */
    if(flag && 0 == strcmp(boolean_string, "True")){
        MPI_Info_free(&info);
        
        v_print(5, "%s: DYN_PSET: Proc is dynamic\n", data->hostname);

        data->is_dynamic = true;
        data->setop_pending = true;
        keys = (char**) malloc(3 * sizeof(char*));
        keys[0] = strdup("dyn_pset");
        keys[1] = strdup("delta_add");
        keys[2] = strdup("delta_sub");
        MPI_Session_get_pset_data (data->session, data->main_pset, data->main_pset, keys, 3, true, &info);
        MPI_Info_get(info, keys[0], MPI_MAX_PSET_NAME_LEN, data->main_pset, &flag);
        MPI_Info_get(info, keys[1], MPI_MAX_PSET_NAME_LEN, data->delta_add, &flag);
        MPI_Info_get(info, keys[2], MPI_MAX_PSET_NAME_LEN, data->delta_sub, &flag);
        free_string_array(&keys, 3);
        v_print(5, "%s: DYN_PSET: got Pset data\n", data->hostname);
        if(!flag){
            printf("No 'next_main_pset' was provided for dynamic process. Terminate.\n");
            dyn_pset_data_free(&data);
            return NULL;
        }
        MPI_Info_free(&info);
        
        /* This process was spawned but the simulation is about to end so this process needs to terminate as well */
        if(0 == strcmp(data->main_pset, "dyn_psets://canceled")){
            v_print(5, "%s: DYN_PSET_INIT: PSetOp was canceled\n", data->hostname);
            if(primary){
                MPI_Info_create(&info);                                                                             
                MPI_Info_set(info, "dyn_psets://canceled", "dyn_psets://canceled/ack");                                                  
                MPI_Session_set_pset_data(data->session, "mpi://WORLD", info);
                MPI_Info_free(&info); 
            }
            dyn_pset_data_free(&data);
            return NULL;
        }

    }else{
        data->is_dynamic = false;
        strcpy(data->main_pset, pset);
    }

    return data;
}

void dyn_pset_data_free(dyn_pset_data_t ** data_ptr){
    dyn_pset_data_t *data = *data_ptr;
    if (!data->config->gc && MPI_INFO_NULL != data->info){
      MPI_Info_free(&data->info);
    }
    if (MPI_REQUEST_NULL != data->request){
      MPI_Request_free(&data->request);
    }
    if (MPI_SESSION_NULL != data->session){
      MPI_Session_finalize(&data->session);
    }
    if(NULL != data->output_psets){
        free_string_array(&data->output_psets, data->noutput_psets);
    }
    dyn_pset_config_free(&data->config);
    free(data);
}

dyn_pset_state_t * dyn_pset_state_new(const char *pset, void * user_pointer,
                            dyn_psets_expand_send_func_t exp_send, dyn_psets_expand_recv_func_t exp_recv,
                            dyn_psets_shrink_send_func_t shr_send, dyn_psets_shrink_recv_func_t shr_recv){
    dyn_pset_state_t * state = malloc(sizeof(dyn_pset_state_t));
    state->mpirank = 0;
    state->mpisize = 0;
    state->is_dynamic = 0;
    state->mpicomm = MPI_COMM_NULL;
    state->group_add = MPI_GROUP_NULL;
    state->group_sub = MPI_GROUP_NULL;
    state->dyn_pset_data = (void *) dyn_pset_data_new(pset, exp_send, exp_recv, shr_send, shr_recv);

    if(NULL == state->dyn_pset_data){
        dyn_pset_state_free(&state);
        return NULL;
    }

    state->user_pointer = user_pointer;
    state->is_dynamic = ((dyn_pset_data_t *)state->dyn_pset_data)->is_dynamic;
    return state;
}
void dyn_pset_state_free(dyn_pset_state_t **state);
void dyn_pset_state_free(dyn_pset_state_t **state){
    if(state && *state){
        if(MPI_COMM_NULL != (*state)->mpicomm){
            MPI_Comm_disconnect(&(*state)->mpicomm);
        }
        if(MPI_GROUP_NULL != (*state)->group_add && MPI_GROUP_EMPTY != (*state)->group_add){
            MPI_Group_free(&(*state)->group_add);
        }
        if(MPI_GROUP_NULL != (*state)->group_sub && MPI_GROUP_EMPTY != (*state)->group_sub){
            MPI_Group_free(&(*state)->group_sub);
        }
        if(NULL != (*state)->dyn_pset_data){
            dyn_pset_data_free((dyn_pset_data_t **) &(*state)->dyn_pset_data);
        }
        free(*state);
        *state = NULL;
    }
}



void free_string_array(char ***array, int size){
    int i;
    if(0 == size){
        *array = NULL;
        return;
    }
    for(i = 0; i < size; i++){
        free((*array)[i]);
    }
    
    free(*array);

    *array = NULL;
}

int v_print(int req_v, char * format, ... ){
    int res = 0;
    va_list args;

    if(req_v > verbosity){
        return res;
    }

    va_start(args, format);
    res = vfprintf(dyn_pset_output, format, args);
    va_end(args);

    fflush(dyn_pset_output);

    return res;
}
