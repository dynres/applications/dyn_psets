#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "dyn_psets.h"
#include "dyn_psets_internal.h"

FILE *dyn_pset_output = NULL;
char dyn_pset_outputname[256] = "";
int verbosity = 0;

void send_setop(dyn_pset_state_t * state){

    char ** input_psets;
    dyn_pset_data_t * data = (dyn_pset_data_t *) state->dyn_pset_data;

    v_print(3, "DYN_PSET: send_setop: called by rank %d \n", state->mpirank);

    input_psets = (char **)malloc(1 * sizeof(char *));                                                                                 
    input_psets[0] = strdup(data->main_pset);                                                                                                
    data->setop = MPI_PSETOP_REPLACE;                                                                                                    
    MPI_Session_dyn_v2a_psetop_nb(data->session, &data->setop, input_psets, 1, &data->output_psets, &data->noutput_psets, data->info, &data->request);                   
    free_string_array(&input_psets, 1);
    data->setop_pending = true;
}

void check_setop(dyn_pset_state_t * state, int blocking, int * flag){
    MPI_Status status;
    MPI_Info info;
    dyn_pset_data_t * data = (dyn_pset_data_t *) state->dyn_pset_data;
    
    v_print(3, "DYN_PSET: check_setop: called by rank %d \n", state->mpirank);
    
    *flag = 0;
    if(blocking){
        MPI_Wait(&data->request, MPI_STATUS_IGNORE);
        *flag = 1;
    }else{
        MPI_Test(&data->request, flag, &status);
    }
    v_print(5, "DYN_PSET: check_setop: %d recived by rank %d \n", *flag, state->mpirank);
    
    if(*flag){
        v_print(5, "DYN_PSET: check_setop: op %d, noutput %d recived by rank %d \n", data->setop, data->noutput_psets, state->mpirank);
        if(data->setop == MPI_PSETOP_REPLACE){
            data->request = MPI_REQUEST_NULL;
            strcpy(data->delta_sub, data->output_psets[0]);
            strcpy(data->delta_add, data->output_psets[1]);
            strcpy(data->main_pset, data->output_psets[2]);
            free_string_array(&data->output_psets, data->noutput_psets);

            MPI_Info_create(&info);                                                                             
            MPI_Info_set(info, "dyn_pset", data->main_pset);
            MPI_Info_set(info, "delta_add", data->delta_add);
            MPI_Info_set(info, "delta_sub", data->delta_sub);

            v_print(5, "DYN_PSET: check_setop: Set data '%s' by rank %d on host %s\n", data->delta_add, state->mpirank, data->hostname);                                                  
            MPI_Session_set_pset_data(data->session, data->delta_add, info);
            v_print(5, "DYN_PSET: check_setop: Done Set data '%s' by rank %d on host %s\n", data->delta_add, state->mpirank, data->hostname);
            
            MPI_Info_free(&info);  
            
            data->noutput_psets = 0;
        }else{
            /* Try again */
            send_setop(state);
            *flag = 0;
        }
    }
}

void handle_setop(dyn_pset_state_t * state, int * terminate, int *reconfigured){

    int flag;
    char boolean_string[10];
    MPI_Group group;
    MPI_Info info;

    dyn_pset_data_t * data = (dyn_pset_data_t *) state->dyn_pset_data;

    *reconfigured = 1;
    data->setop_pending = false;

    v_print(3, "DYN_PSET: handle_setop: called by rank %d \n", state->mpirank);

    /* Distribute the new pset names */
    MPI_Bcast(data->delta_add, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, state->mpicomm);
    MPI_Bcast(data->delta_sub, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, state->mpicomm);
    MPI_Bcast(data->main_pset, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, state->mpicomm);

    /* Check if this process is included in the new PSet */                                        
    MPI_Session_get_pset_info(data->session, data->main_pset, &info);                                                                            
    MPI_Info_get(info, "mpi_included", 6, boolean_string, &flag);                                                                      
    MPI_Info_free(&info);

    /* Create new add/sub groups */
    if(MPI_GROUP_NULL != state->group_add && MPI_GROUP_EMPTY != state->group_add){
        MPI_Group_free(&state->group_add);
    }
    if(MPI_GROUP_NULL != state->group_sub && MPI_GROUP_EMPTY != state->group_sub){
        MPI_Group_free(&state->group_sub);
    }
    MPI_Group_from_session_pset (data->session, data->delta_add, &state->group_add);
    MPI_Group_from_session_pset (data->session, data->delta_sub, &state->group_sub);

    if(0 != strcmp("", data->delta_sub)){
        if(0 != strcmp(boolean_string, "True")){

            /* Send away our data before we leave */
            if(NULL != data->shr_send){
                data->shr_send(state);
            }
            *terminate = 1;
            state->mpirank = -1;
            data->rank = -1;
            return;
        }
        if(NULL != data->shr_recv){
            data->shr_recv(state);
        }
    }
    if(data->config->gc){
        MPI_Comm_disconnect(&state->mpicomm);
    }

    v_print(3, "DYN_PSET: handle_setop: create comm from %s by rank %d on host %s\n", data->main_pset, state->mpirank, data->hostname);
    /* Create a new comm */
    MPI_Group_from_session_pset (data->session, data->main_pset, &group);
    MPI_Comm_create_from_group(group, data->main_pset, MPI_INFO_NULL, MPI_ERRORS_RETURN, &state->mpicomm);
    MPI_Group_free(&group);
    v_print(3, "DYN_PSET: handle_setop: created comm from %s by rank %d on host %s\n", data->main_pset, state->mpirank, data->hostname);
    /* Store new mpi rank */
    MPI_Comm_rank(state->mpicomm, &data->rank);
    state->mpirank = data->rank;

    /* Store the new size */
    MPI_Comm_size(state->mpicomm, &state->mpisize);

    if(0 != strcmp("", data->delta_add)){
        /* Pack and send the user data */
        if(NULL != data->exp_send){
            data->exp_send(state);
        }
    }

    /* Rank 0 of new comm finalizes the set operation*/
    if(data->rank == 0){
        //v_print(2, "%s: DYN_PSET: handle setop: finalize setop %s \n", data->hostname, data->main_pset);
        MPI_Session_dyn_finalize_psetop(data->session, data->main_pset);
    }
}

dyn_pset_state_t * dyn_pset_init(const char *initial_pset, void *user_pointer, MPI_Info info,
                    dyn_psets_expand_send_func_t exp_send, dyn_psets_expand_recv_func_t exp_recv,
                    dyn_psets_shrink_send_func_t shr_send, dyn_psets_shrink_recv_func_t shr_recv){

    MPI_Group group;
    MPI_Info pset_info;
    int rc, flag;
    char pset_alias[MPI_MAX_PSET_NAME_LEN];

    dyn_pset_output = stdout;

    v_print(2, "DYN_PSET: dyn_pset_init: called\n");
    v_print(5, "DYN_PSET: dyn_pset_init: dyn_pset_state_new\n");
    dyn_pset_state_t *dyn_pset_state = dyn_pset_state_new(initial_pset, user_pointer, exp_send, exp_recv, shr_send, shr_recv);
    v_print(5, "DYN_PSET: dyn_pset_init: dyn_pset_state_new finished\n");
    if(NULL == dyn_pset_state){
        v_print(2, "DYN_PSET: dyn_pset_init: dyn_pset_state_new retured NULL\n");
        return NULL;
    }
    dyn_pset_data_t * dyn_pset_data = (dyn_pset_data_t *) dyn_pset_state->dyn_pset_data;

    if(MPI_INFO_NULL != info){
        dyn_pset_set_info(dyn_pset_state, info);
    }

    MPI_Session_get_pset_info (dyn_pset_data->session, dyn_pset_data->main_pset, &pset_info);
    MPI_Info_get(pset_info, "mpi_alias", MPI_MAX_PSET_NAME_LEN, pset_alias, &flag);
    if(!flag){
        dyn_pset_state_free(&dyn_pset_state);
        return NULL;
    }

    v_print(5, "DYN_PSET: dyn_pset_init: create group\n");
    /* create a group from pset */
    rc = MPI_Group_from_session_pset (dyn_pset_data->session, dyn_pset_data->main_pset, &group);
    v_print(5, "DYN_PSET: dyn_pset_init: create group returned %d\n", rc);
    rc = MPI_Group_from_session_pset (dyn_pset_data->session, dyn_pset_data->delta_add, &dyn_pset_state->group_add);
    if(rc != MPI_SUCCESS){
        printf("Error creating add group\n");
    }
    rc = MPI_Group_from_session_pset (dyn_pset_data->session, dyn_pset_data->delta_sub, &dyn_pset_state->group_sub);
    if(rc != MPI_SUCCESS){
        printf("Error creating add group\n");
    }    
    /* create a communicator from group */
    v_print(5, "DYN_PSET: dyn_pset_init: create comm from %s (%s) by rank %d on host %s\n", dyn_pset_data->main_pset, pset_alias, dyn_pset_state->mpirank, dyn_pset_data->hostname);
    rc = MPI_Comm_create_from_group(group, pset_alias, MPI_INFO_NULL, MPI_ERRORS_RETURN, &dyn_pset_state->mpicomm);
    v_print(5, "DYN_PSET: dyn_pset_init: create commcreate comm from %s (%s) by rank %d on host %s returned %d\n", dyn_pset_data->main_pset, pset_alias, dyn_pset_state->mpirank, dyn_pset_data->hostname, rc);
    MPI_Group_free(&group);
    /* Store our rank */
    MPI_Comm_rank(dyn_pset_state->mpicomm, &dyn_pset_data->rank);
    dyn_pset_state->mpirank = dyn_pset_data->rank;

    /* Store our size */
    MPI_Comm_size(dyn_pset_state->mpicomm, &dyn_pset_state->mpisize);

    if(dyn_pset_state->is_dynamic){
        /* Recv data */
        if(NULL != dyn_pset_data->exp_recv){
            dyn_pset_data->exp_recv(dyn_pset_state);
        }
    }

    /* If this was the launch of the original processes we send a set operation right away  
     * If this was a dynamic launch we might need to finalize the set operation
     */
    if(!dyn_pset_state->is_dynamic){

        /* only send a setop if the user provided an info object */
        if(dyn_pset_data->info != MPI_INFO_NULL){
            dyn_pset_data->setop_pending = true;
            if(dyn_pset_data->rank == 0){
                v_print(2, "DYN_PSET: dyn_pset_init: send setop\n");
                send_setop(dyn_pset_state);
                v_print(2, "DYN_PSET: dyn_pset_init: sent setop\n");
            }
        }

    }else{
        if(dyn_pset_data->info == MPI_INFO_NULL){
            dyn_pset_data->setop_pending = false;
        }
        if(dyn_pset_data->rank == 0){
            //v_print(2, "%s: DYN_PSET: dyn_pset_init: finalize setop %s \n", dyn_pset_data->hostname, dyn_pset_data->main_pset);
            MPI_Session_dyn_finalize_psetop(dyn_pset_data->session, dyn_pset_data->main_pset);
        }        
    }

    v_print(2, "DYN_PSET: dyn_pset_init: return \n");
    return dyn_pset_state;

}

int dyn_pset_adapt(dyn_pset_state_t * state, int *terminate, int *reconfigured){
    int flag;

    dyn_pset_data_t *dyn_pset_data = (dyn_pset_data_t *) state->dyn_pset_data;

    *terminate = 0;
    *reconfigured = 0;

    v_print(2, "DYN_PSET: dyn_pset_adapt: called by rank %d \n", state->mpirank);

    if(!dyn_pset_data->setop_pending){
        if(dyn_pset_data->info != MPI_INFO_NULL){                                                                                                                                
            if (dyn_pset_data->rank == 0)                                                                                    
            {                                                                                                                                      
                send_setop(state);                                                                                                         
            }
            dyn_pset_data->setop_pending = true;
        }
    }

    if(dyn_pset_data->setop_pending){
        /* Rank 0 requested the setop, so it does the check and Bcasts the result */
        if(dyn_pset_data->rank == 0){
            check_setop(state, 1, &flag);
        }
        MPI_Bcast(&flag, 1, MPI_INT, 0, state->mpicomm);

        /* Setop still pending, so bail out here */
        if(!flag){
            return 0;
        }
        handle_setop(state, terminate, reconfigured);
        if(*terminate){
            return 0;
        }
    }

    return 0; 
}

int dyn_pset_adapt_nb(dyn_pset_state_t * state, int *terminate, int *reconfigured){
    int flag = 0;

    //v_print(2, "DYN_PSET: dyn_pset_adapt_nb: called by rank %d \n", state->mpirank);

    dyn_pset_data_t *dyn_pset_data = (dyn_pset_data_t *) state->dyn_pset_data;

    *terminate = 0;
    *reconfigured = 0;

    /* If we have a pending setop we need to check if it was executed 
     * and if so adapt accordingly
     */
    if(dyn_pset_data->setop_pending){

        /* Rank 0 requested the setop, so it does the check and Bcasts the result */
        if(dyn_pset_data->rank == 0){
            check_setop(state, 0, &flag);
        }

        MPI_Bcast(&flag, 1, MPI_INT, 0, state->mpicomm);
        /* Setop still pending, so bail out here */
        if(!flag){
            return 0;
        }

        handle_setop(state, terminate, reconfigured);


        if(*terminate){
            return 0;
        }
    }
    if(!dyn_pset_data->setop_pending){                                                                                                                                
        if (dyn_pset_data->rank == 0)                                                                                    
        {                                                                                                                                      
            send_setop(state);                                                                                                         
        }

        dyn_pset_data->setop_pending = true;
    } 
    return 0;

}                                                                                                                                      
     
int dyn_pset_finalize(dyn_pset_state_t ** state, char *final_pset){

    char **input_psets, **output_psets;
    char *key = "dyn_psets://canceled";
    char ack_response[MPI_MAX_PSET_NAME_LEN];
    char _final_pset[256];
    char sub_pset[256] = "";
    char cancel_pset[256] = ""; 
    char boolean_string[64] = "";
    char *_final_pset_ptr;
    int flag, setop, term = 0, noutput = 0;
    MPI_Info info;

    dyn_pset_state_t *dyn_pset_state = *state;
    dyn_pset_data_t *data = dyn_pset_state->dyn_pset_data;

    strcpy(_final_pset, data->main_pset);

    /* Cancel any outstanding PSetOps */
    if (data->setop_pending){
        if(data->rank == 0){
            /* Send the setop cancelation - op will be MPI_PSETOP_NULL if setop already applied */
            setop = MPI_PSETOP_CANCEL;                                                                                     
            input_psets = (char **)malloc(1 * sizeof(char *));                                                              
            input_psets[0] = strdup(data->main_pset);                                                                             
            noutput = 0;
            MPI_Session_dyn_v2a_psetop(data->session, &setop, input_psets, 1, &output_psets, &noutput, MPI_INFO_NULL);       
            free_string_array(&input_psets, 1);
            free_string_array(&output_psets, noutput);
            /* If cancelation did not succeed finalize outstanding setop and store pset names */
            if (MPI_PSETOP_NULL == setop)                                                                                  
            {
                MPI_Wait(&data->request, MPI_STATUS_IGNORE);
                if (MPI_PSETOP_NULL != data->setop)                                                                            
                {                                                                                                           
                    strcpy(sub_pset, data->output_psets[0]);                                                                                                       
                    strcpy(cancel_pset, data->output_psets[1]);
                    strcpy(_final_pset, data->output_psets[2]);                                                              
                    MPI_Session_dyn_finalize_psetop(data->session, data->main_pset);          
                    free_string_array(&data->output_psets, data->noutput_psets);                                            
                }                                                                       
            }
        }
        
        /* Rank 0 xcasts cancelation results */
        MPI_Bcast(sub_pset, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, (*state)->mpicomm);
        MPI_Bcast(_final_pset, MPI_MAX_PSET_NAME_LEN, MPI_CHAR, 0, (*state)->mpicomm);

        /* If sub Pset is not empty, all members of this set need to terminate before SUB */
        if(0 != strcmp(sub_pset, "")){
            MPI_Session_get_pset_info (data->session, sub_pset, &info);
            MPI_Info_get(info, "mpi_included", 63, boolean_string, &flag);
            term = (flag && 0 == strcmp(boolean_string, "True"));
            MPI_Info_free(&info);
            MPI_Comm_disconnect(&(*state)->mpicomm);
            (*state)->mpicomm = MPI_COMM_NULL;
        }
    }

    /* Send a SUB operation to delete this PSet and inform potential spawned processes about cancelation */
    if(data->rank == 0){
        setop = MPI_PSETOP_SUB;
        input_psets = (char **)malloc(1 * sizeof(char *));                                                              
        input_psets[0] = strdup(_final_pset);
        noutput = 0;
        MPI_Info_create(&info);
        MPI_Info_set(info, "model", "DefaultSubModel()");
        MPI_Info_set(info, "output_space_generator", "output_space_generator_sub");
        MPI_Info_set(info, "input_pset_models_0", "NullPSetModel()");   
        
        MPI_Session_dyn_v2a_psetop(data->session, &setop, input_psets, 1, &output_psets, &noutput, info);
        
        MPI_Info_free(&info);
        free_string_array(&input_psets, 1);
        free_string_array(&output_psets, noutput);

         /* If we have a cancel PSet, inform spawned processes about cancelation */
        if(0 != strcmp(cancel_pset, "")){
            MPI_Info_create(&info);                                                                             
            MPI_Info_set(info, "dyn_pset", "dyn_psets://canceled");
            MPI_Info_set(info, "delta_add", "dyn_psets://canceled");
            MPI_Info_set(info, "delta_sub", "dyn_psets://canceled");                                                  
            MPI_Session_set_pset_data(data->session, cancel_pset, info);
            MPI_Info_free(&info);
            /* Wait for acknowledgement of spawned processes */
            MPI_Session_get_pset_data (data->session, "mpi://SELF", cancel_pset, &key, 1, true, &info);
            MPI_Info_get(info, key, MPI_MAX_PSET_NAME_LEN, ack_response, &flag);
            if(!flag){
                printf("ERROR: No ack received from spawned processes\n");
                dyn_pset_state_free(state);
                return 1;
            }
            MPI_Info_free(&info); 
        }       
    }

    /* If we have a pending shrink make sure SUB operation is executed before terminating */
    if(0 != strcmp(sub_pset, "") && !term){
        _final_pset_ptr = malloc(MPI_MAX_PSET_NAME_LEN);
        strcpy(_final_pset_ptr, _final_pset);
        MPI_Session_pset_barrier(data->session, (char **) &_final_pset_ptr, 1, MPI_INFO_NULL);     
        free(_final_pset_ptr);
    }

    /* Finalize the dyn_pset state - this calls mpi_comm_disconnect if not pending shrink */
    dyn_pset_state_free(state);

    /* Close opened output files */
    if(stdout != dyn_pset_output){
        fclose(dyn_pset_output);
    }

    if(NULL != final_pset){
        strcpy(final_pset, _final_pset);
    }

    return 0;
}

int dyn_pset_set_info(dyn_pset_state_t * state, MPI_Info info){
    dyn_pset_data_t *data = (dyn_pset_data_t *) state->dyn_pset_data;

    if(data->info != MPI_INFO_NULL){
        MPI_Info_free(&data->info);
    }

    if(info == MPI_INFO_NULL){
        data->info = MPI_INFO_NULL;
    }else{
        MPI_Info_dup(info, &data->info);
    }

    return 0;
}

int dyn_pset_config(dyn_pset_state_t * state, const char * confiq_param, void * value){
    bool *b;
    dyn_pset_data_t *dyn_pset_data;
    dyn_pset_config_t *dyn_pset_config;

    dyn_pset_data = state->dyn_pset_data;
    dyn_pset_config = dyn_pset_data->config;
    
    if (0 == strcmp(confiq_param, "garbage_collection")){
        b = (bool *) value;
        dyn_pset_config->gc = *b;
    }

    return 0;
}

void dyn_pset_set_output(char * filename, int verbosity_level){
    if(NULL != filename){
        strncpy(dyn_pset_outputname, filename, 255);
        dyn_pset_output = fopen(dyn_pset_outputname, "a+");
    }

    verbosity = verbosity_level;
}