#ifndef DYN_PSETS_INTERNAL_H
#define DYN_PSETS_INTERNAL_H

#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

#include "mpi.h"
#include "dyn_psets_common.h"

extern FILE *dyn_pset_output;
extern char dyn_pset_outputname[256];
extern int verbosity;

typedef struct dyn_psets_config
{
  bool gc;
}dyn_pset_config_t;

typedef struct dyn_pset_data{
  char delta_add[MPI_MAX_PSET_NAME_LEN];
  char delta_sub[MPI_MAX_PSET_NAME_LEN];
  char main_pset[MPI_MAX_PSET_NAME_LEN];
  MPI_Session session;
  MPI_Info info;
  MPI_Request request;
  char **output_psets;
  int noutput_psets;
  int setop;
  int rank;
  bool setop_pending;
  bool is_dynamic;
  dyn_psets_expand_send_func_t exp_send;
  dyn_psets_expand_recv_func_t exp_recv;
  dyn_psets_shrink_send_func_t shr_send; 
  dyn_psets_shrink_recv_func_t shr_recv;
  dyn_pset_config_t * config;
  char hostname[256];
}dyn_pset_data_t;

dyn_pset_data_t * dyn_pset_data_new(const char *pset, dyn_psets_expand_send_func_t exp_send, dyn_psets_expand_recv_func_t exp_recv, dyn_psets_shrink_send_func_t shr_send, dyn_psets_shrink_recv_func_t shr_recv);
void dyn_pset_data_free(dyn_pset_data_t ** data_ptr);

dyn_pset_state_t * dyn_pset_state_new(const char *pset, void *user_pointer, dyn_psets_expand_send_func_t exp_send, dyn_psets_expand_recv_func_t exp_recv,
           dyn_psets_shrink_send_func_t shr_send, dyn_psets_shrink_recv_func_t shr_recv);
void dyn_pset_state_free(dyn_pset_state_t **state);


void free_string_array(char ***array, int size);
int v_print(int req_v, char * format, ... );


#endif /* !DYN_PSETS_INTERNAL_H */
