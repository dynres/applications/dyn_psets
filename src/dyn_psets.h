#ifndef DYN_PSETS_H
#define DYN_PSETS_H

#include "mpi.h"
#include "dyn_psets_common.h"
#include <stdbool.h>


dyn_pset_state_t * dyn_pset_init(const char *initial_pset, void *user_pointer, MPI_Info info,
           dyn_psets_expand_send_func_t exp_send, dyn_psets_expand_recv_func_t exp_recv,
           dyn_psets_shrink_send_func_t shr_send, dyn_psets_shrink_recv_func_t shr_recv);
           
int dyn_pset_adapt(dyn_pset_state_t * state, int *terminate, int *reconfigured);
int dyn_pset_adapt_nb(dyn_pset_state_t * state, int *terminate, int *reconfigured);

int dyn_pset_finalize(dyn_pset_state_t ** state, char *final_pset);

int dyn_pset_set_info(dyn_pset_state_t * state, MPI_Info info);
void dyn_pset_set_output(char * filename, int verbosity_level);
int dyn_pset_config(dyn_pset_state_t * state, const char * confiq_param, void * value);

#endif /* !DYN_PSETS_H */
