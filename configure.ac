#!/bin/sh

AC_INIT([dyn_psets], [1.0], [domi.huber@tum.de])
AM_INIT_AUTOMAKE
AC_PROG_CC


# Add the --with-mpi option
AC_ARG_WITH([mpi],
  [AS_HELP_STRING([--with-mpi],
    [Specify the path to mpi library and include directory])],
  [MPI_PATH="$withval"],
  [MPI_PATH=""])

# Check if the mpi path is provided
if test "x$MPI_PATH" != "x"; then
  AC_MSG_NOTICE([Using user-specified mpi path: $MPI_PATH])
else
  AC_MSG_NOTICE([No mpi path specified, using default])
  # Set a default path or behavior if not specified
  MPI_PATH="/usr/local"
fi

# Enable debugging option
AC_ARG_WITH([debug],
  [AS_HELP_STRING([--with-debug],
  [Enable debugging support (no,yes,sanitize) @<:@default=no@:>@])],
  [with_debug=$withval],
  [with_debug=no])

# Define debug level
if test "$with_debug" = "yes"; then
  AM_CFLAGS="-g -Wall"
  AC_DEFINE([DEBUG], [1], [Enable debug symbols])
elif test "$with_debug" = "sanitize"; then
  AM_CFLAGS="-fsanitize=address -g -Wall"
  AC_DEFINE([DEBUG], [2], [Enable debug symbols with address sanitizer])
else
  AM_CFLAGS="-O3"
  AC_DEFINE([DEBUG], [0], [Disable debug symbols])
fi

# Add the specified mpi library and include paths to AM_LDFLAGS and AM_CPPFLAGS
AM_LDFLAGS="-L$MPI_PATH/lib $AM_LDFLAGS"
AM_CFLAGS="$AM_CFLAGS -I$MPI_PATH/include"
AC_SUBST([MPI_PATH])
AC_SUBST([AM_CFLAGS])
AC_SUBST([AM_LDFLAGS])


# Define the installation prefix
AC_PREFIX_DEFAULT([/usr/local])

# Checks for libraries
AC_CHECK_LIB([mpi], [MPI_Session_dyn_v2a_psetop])

# Output Makefile
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
